from abc import ABCMeta, abstractmethod


class Member(metaclass=ABCMeta):
    def __init__(self, id: int, software):
        self.id = id
        self.software = software

    def attach_member(self):
        print(f"Member with id={self.id} is attached")
        self.software.members.append(self.id)

    def detach_member(self):
        print(f"Member with id={self.id} id detached")
        self.software.members.remove(self.id)

    @abstractmethod
    def work_with_software(self):
        pass

class SoftwareArchitect(Member):
    def __init__(self, id, software):
        super(SoftwareArchitect, self).__init__(id, software)

    def work_with_software(self):
        self.attach_member()

        self.software.create_request(self.id)
        self.software.add_features()
        self.detach_member()

class Developer(Member):
    def __init__(self, id, software):
        super(Developer, self).__init__(id, software)

    def work_with_software(self):
        self.attach_member()

        self.software.create_request(self.id)
        self.software.add_features()
        self.detach_member()


class TeamLead(Member):
    def __init__(self, id, software):
        super(TeamLead, self).__init__(id, software)

    def work_with_software(self):
        self.attach_member()
        print(f"{self.__class__.__name__} with id={self.id} is working")

        self.software.create_request(self.id, )
        self.software.add_features()
        self.detach_member()


class QA(Member):
    def __init__(self, id, software):
        super(QA, self).__init__(id, software)

    def work_with_software(self):
        self.attach_member()
        self.software.create_request(self.id)
        self.software.add_features()
        self.detach_member()


class BusinessAnalyst(Member):
    def __init__(self, id, software):
        super(BusinessAnalyst, self).__init__(id, software)

    def work_with_software(self):
        self.attach_member()
        print(f"{self.__class__.__name__} with id={self.id} is working")

        self.software.create_request(self.id)
        self.software.add_features()
        self.detach_member()
        

    