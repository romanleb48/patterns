from personalinfo import PersonalInfo
from software import *
from team import *
from abc import abstractmethod
from dataclasses import dataclass

        
software = JSFrontend(id=1)
developer = Developer(id=1, software=software)

print()

qa = QA(id=2, software=software)
qa.work_with_software()
