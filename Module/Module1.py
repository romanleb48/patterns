from abc import ABC
import random


class Game:
    def init(self, rules: str, team: str):
        self.rules = rules
        self.team = team
        
    def start(self):
        return print("Game started, white make a move")

    def end(end: str):
        team = ["White", "Black"]
        return print(f"Checkmate,", random.choice(team), "wins")


class Piece(ABC):
    def init(self, team: str, figure: str,):
        self.team = team
        self.figure = figure

    def get_figure(self):
        return self.figure

    def get_team(self):
        self.team = ["White", "Black"]
        return self.team

    def move(move: str):
        team = ["White", "Black"]
        figure = ["Pawn","Queen", "King", "Horse"]
        return print(random.choice(team),random.choice(figure),"figure make a move")


class Board():
    def init(self, board: dict):
        self.board = board

if __name__ == '__main__':
    Game.start(str)
    Piece.move(str)
    Game.end(str)