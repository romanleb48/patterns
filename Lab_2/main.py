from Employees import Developer, ProjectManager
from Project import Project
from datetime import datetime
from PersonalInfo import PersonalInfo

if __name__ == '__main__':
    developer = Developer(PersonalInfo(id=1, name='test', address='test', phone='test',
                            email='test@test.test', position='junior', rank='0.7', salary=54000.00))

    project = Project(title='test', start_date=datetime.now())
    developer.assign(project)

    developer.assigned_projects(developer)
    developer.unassign(project)
    developer.assign_possibility(project)

    ProjectManager.discuss_progress(developer)

    #developer.unassign(project)
    #developer.assign_possibility(project)

    #project.add_developer('Morgan')
    #for x in project.developers: print(x)
    #project.remove_developer('Morgan')
    #for x in project.developers: print(x)

